PK     @��               node_modules/PK     @��            +   node_modules/unist-util-stringify-position/PK
     @��Р,K  K  2   node_modules/unist-util-stringify-position/license(The MIT License)

Copyright (c) 2016 Titus Wormer <tituswormer@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
PK
     @���1�    3   node_modules/unist-util-stringify-position/index.js/**
 * @typedef {import('unist').Point} Point
 * @typedef {import('unist').Node} Node
 * @typedef {import('unist').Position} Position
 * @typedef {object & {type: string, position?: Position|undefined}} NodeLike
 */

/**
 * Stringify one point, a position (start and end points), or a node’s
 * positional information.
 *
 * @param {Node|NodeLike|Position|Point|null} [value]
 * @returns {string}
 */
export function stringifyPosition(value) {
  // Nothing.
  if (!value || typeof value !== 'object') {
    return ''
  }

  // Node.
  if ('position' in value || 'type' in value) {
    return position(value.position)
  }

  // Position.
  if ('start' in value || 'end' in value) {
    return position(value)
  }

  // Point.
  if ('line' in value || 'column' in value) {
    return point(value)
  }

  // ?
  return ''
}

/**
 * @param {Point|undefined} point
 * @returns {string}
 */
function point(point) {
  return index(point && point.line) + ':' + index(point && point.column)
}

/**
 * @param {Position|undefined} pos
 * @returns {string}
 */
function position(pos) {
  return point(pos && pos.start) + '-' + point(pos && pos.end)
}

/**
 * @param {number|undefined} value
 * @returns {number}
 */
function index(value) {
  return value && typeof value === 'number' ? value : 1
}
PK
     @�����    7   node_modules/unist-util-stringify-position/package.json{
  "name": "unist-util-stringify-position",
  "version": "3.0.2",
  "description": "unist utility to serialize a node, position, or point as a human readable location",
  "license": "MIT",
  "keywords": [
    "unist",
    "unist-util",
    "util",
    "utility",
    "position",
    "location",
    "point",
    "node",
    "stringify",
    "tostring"
  ],
  "repository": "syntax-tree/unist-util-stringify-position",
  "bugs": "https://github.com/syntax-tree/unist-util-stringify-position/issues",
  "funding": {
    "type": "opencollective",
    "url": "https://opencollective.com/unified"
  },
  "author": "Titus Wormer <tituswormer@gmail.com> (https://wooorm.com)",
  "contributors": [
    "Titus Wormer <tituswormer@gmail.com> (https://wooorm.com)"
  ],
  "sideEffects": false,
  "type": "module",
  "main": "index.js",
  "types": "index.d.ts",
  "files": [
    "index.d.ts",
    "index.js"
  ],
  "dependencies": {
    "@types/unist": "^2.0.0"
  },
  "devDependencies": {
    "@types/mdast": "^3.0.0",
    "@types/tape": "^4.0.0",
    "c8": "^7.0.0",
    "prettier": "^2.0.0",
    "remark-cli": "^10.0.0",
    "remark-preset-wooorm": "^9.0.0",
    "rimraf": "^3.0.0",
    "tape": "^5.0.0",
    "type-coverage": "^2.0.0",
    "typescript": "^4.0.0",
    "xo": "^0.48.0"
  },
  "scripts": {
    "prepack": "npm run build && npm run format",
    "build": "rimraf \"*.d.ts\" && tsc && type-coverage",
    "format": "remark . -qfo && prettier . -w --loglevel warn && xo --fix",
    "test-api": "node test.js",
    "test-coverage": "c8 --check-coverage --branches 100 --functions 100 --lines 100 --statements 100 --reporter lcov node test.js",
    "test": "npm run build && npm run format && npm run test-coverage"
  },
  "prettier": {
    "tabWidth": 2,
    "useTabs": false,
    "singleQuote": true,
    "bracketSpacing": false,
    "semi": false,
    "trailingComma": "none"
  },
  "xo": {
    "prettier": true
  },
  "remarkConfig": {
    "plugins": [
      "preset-wooorm"
    ]
  },
  "typeCoverage": {
    "atLeast": 100,
    "detail": true,
    "strict": true
  }
}
PK
     @��J��  �  4   node_modules/unist-util-stringify-position/readme.md# unist-util-stringify-position

[![Build][build-badge]][build]
[![Coverage][coverage-badge]][coverage]
[![Downloads][downloads-badge]][downloads]
[![Size][size-badge]][size]
[![Sponsors][sponsors-badge]][collective]
[![Backers][backers-badge]][collective]
[![Chat][chat-badge]][chat]

**[unist][]** utility to pretty print the positional information of a node.

## Contents

*   [What is this?](#what-is-this)
*   [When should I use this?](#when-should-i-use-this)
*   [Install](#install)
*   [Use](#use)
*   [API](#api)
    *   [`stringifyPosition(node|position|point)`](#stringifypositionnodepositionpoint)
*   [Types](#types)
*   [Compatibility](#compatibility)
*   [Security](#security)
*   [Related](#related)
*   [Contribute](#contribute)
*   [License](#license)

## What is this?

This package is a utility that takes any [unist][] (whether mdast, hast, etc)
node, position, or point, and serializes its positional info.

## When should I use this?

This utility is useful to display where something occurred in the original
document, in one standard way, for humans.
For example, when throwing errors or warning messages about something.

## Install

This package is [ESM only][esm].
In Node.js (version 12.20+, 14.14+, or 16.0+), install with [npm][]:

```sh
npm install unist-util-stringify-position
```

In Deno with [`esm.sh`][esmsh]:

```js
import {stringifyPosition} from 'https://esm.sh/unist-util-stringify-position@3'
```

In browsers with [`esm.sh`][esmsh]:

```html
<script type="module">
  import {stringifyPosition} from 'https://esm.sh/unist-util-stringify-position@3?bundle'
</script>
```

## Use

```js
import {stringifyPosition} from 'unist-util-stringify-position'

stringifyPosition({line: 2, column: 3}) // => '2:3' (point)
stringifyPosition({start: {line: 2}, end: {line: 3}}) // => '2:1-3:1' (position)
stringifyPosition({
  type: 'text',
  value: '!',
  position: {
    start: {line: 5, column: 11},
    end: {line: 5, column: 12}
  }
}) // => '5:11-5:12' (node)
```

## API

This package exports the identifier `stringifyPosition`.
There is no default export.

### `stringifyPosition(node|position|point)`

Stringify a [point][], [position][], or a [node][].

###### Parameters

*   `node` ([`Node`][node])
    — node whose `'position'` property to stringify
*   `position` ([`Position`][position])
    — position whose `'start'` and `'end'` points to stringify
*   `point` ([`Point`][point])
    — point whose `'line'` and `'column'` to stringify

###### Returns

`string?` — A range `ls:cs-le:ce` (when given `node` or `position`) or a point
`l:c` (when given `point`), where `l` stands for line, `c` for column, `s` for
`start`, and `e` for end.
An empty string (`''`) is returned if the given value is neither `node`,
`position`, nor `point`.

## Types

This package is fully typed with [TypeScript][].
There are no additional types exported.

## Compatibility

Projects maintained by the unified collective are compatible with all maintained
versions of Node.js.
As of now, that is Node.js 12.20+, 14.14+, and 16.0+.
Our projects sometimes work with older versions, but this is not guaranteed.

## Security

This project is safe.

## Related

*   [`unist-util-generated`](https://github.com/syntax-tree/unist-util-generated)
    — check if a node is generated
*   [`unist-util-position`](https://github.com/syntax-tree/unist-util-position)
    — get positional info of nodes
*   [`unist-util-remove-position`](https://github.com/syntax-tree/unist-util-remove-position)
    — remove positional info from trees
*   [`unist-util-source`](https://github.com/syntax-tree/unist-util-source)
    — get the source of a value (node or position) in a file

## Contribute

See [`contributing.md` in `syntax-tree/.github`][contributing] for ways to get
started.
See [`support.md`][support] for ways to get help.

This project has a [code of conduct][coc].
By interacting with this repository, organization, or community you agree to
abide by its terms.

## License

[MIT][license] © [Titus Wormer][author]

<!-- Definition -->

[build-badge]: https://github.com/syntax-tree/unist-util-stringify-position/workflows/main/badge.svg

[build]: https://github.com/syntax-tree/unist-util-stringify-position/actions

[coverage-badge]: https://img.shields.io/codecov/c/github/syntax-tree/unist-util-stringify-position.svg

[coverage]: https://codecov.io/github/syntax-tree/unist-util-stringify-position

[downloads-badge]: https://img.shields.io/npm/dm/unist-util-stringify-position.svg

[downloads]: https://www.npmjs.com/package/unist-util-stringify-position

[size-badge]: https://img.shields.io/bundlephobia/minzip/unist-util-stringify-position.svg

[size]: https://bundlephobia.com/result?p=unist-util-stringify-position

[sponsors-badge]: https://opencollective.com/unified/sponsors/badge.svg

[backers-badge]: https://opencollective.com/unified/backers/badge.svg

[collective]: https://opencollective.com/unified

[chat-badge]: https://img.shields.io/badge/chat-discussions-success.svg

[chat]: https://github.com/syntax-tree/unist/discussions

[npm]: https://docs.npmjs.com/cli/install

[license]: license

[author]: https://wooorm.com

[esm]: https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c

[esmsh]: https://esm.sh

[typescript]: https://www.typescriptlang.org

[contributing]: https://github.com/syntax-tree/.github/blob/HEAD/contributing.md

[support]: https://github.com/syntax-tree/.github/blob/HEAD/support.md

[coc]: https://github.com/syntax-tree/.github/blob/HEAD/code-of-conduct.md

[unist]: https://github.com/syntax-tree/unist

[node]: https://github.com/syntax-tree/unist#node

[position]: https://github.com/syntax-tree/unist#position

[point]: https://github.com/syntax-tree/unist#point
PK
     @��i�ݏ�  �  5   node_modules/unist-util-stringify-position/index.d.ts/**
 * @typedef {import('unist').Point} Point
 * @typedef {import('unist').Node} Node
 * @typedef {import('unist').Position} Position
 * @typedef {object & {type: string, position?: Position|undefined}} NodeLike
 */
/**
 * Stringify one point, a position (start and end points), or a node’s
 * positional information.
 *
 * @param {Node|NodeLike|Position|Point|null} [value]
 * @returns {string}
 */
export function stringifyPosition(
  value?:
    | import('unist').Point
    | import('unist').Node<import('unist').Data>
    | import('unist').Position
    | NodeLike
    | null
    | undefined
): string
export type Point = import('unist').Point
export type Node = import('unist').Node
export type Position = import('unist').Position
export type NodeLike = object & {
  type: string
  position?: Position | undefined
}
/**
 * @param {Position|undefined} pos
 * @returns {string}
 */
declare function position(pos: Position | undefined): string
export {}
PK?     @��                       �A    node_modules/PK?     @��            +           �A+   node_modules/unist-util-stringify-position/PK?
     @��Р,K  K  2           ��t   node_modules/unist-util-stringify-position/licensePK?
     @���1�    3           ��  node_modules/unist-util-stringify-position/index.jsPK?
     @�����    7           ��h
  node_modules/unist-util-stringify-position/package.jsonPK?
     @��J��  �  4           ���  node_modules/unist-util-stringify-position/readme.mdPK?
     @��i�ݏ�  �  5           ���)  node_modules/unist-util-stringify-position/index.d.tsPK        �-    